function err(){
    return "Data Tidak Di Temukan";
}

function sortData1(data) {

    // Inisialisasi array kosong 
    const result = [];
    // Looping untuk mengeluarkan data array
    for (let  i = 0; i < data.length; i++) {
      // age nya dibawah 30 tahun dan favorit buah nya pisang

      if(data[i].age <= 30 && data[i].favoriteFruit === "banana"){

          result.push(data[i]);

      }

    }
    // Pengecekan apakah result memiliki isi
    if (result.length) {
        // mengembalikan nilai dataArray diubah ke json
        // return JSON.stringify(result);
        // Mengembalikan nilai array karena akan di tampilkan di table
        return result;
    }else{
        // menjalan function err
        return err();
    }

}
function sortData2(data){

    // Inisialisasi array kosong 
    const result = [];
    // Looping untuk mengeluarkan data array
    for (let  i = 0; i < data.length; i++) {
      // gender nya female atau company nya FSW4 dan age nya diatas 30 tahun 
      if(data[i].gender === "female" && data[i].company === "FSW4"){
        
        if (data[i].age >= 30){
          result.push(data[i]);
        }
      }

    }
    // Pengecekan apakah result memiliki isi
     if (result.length) {
        // mengembalikan nilai dataArray diubah ke json
        // return JSON.stringify(result);
        // Mengembalikan nilai array karena akan di tampilkan di table
        return result;

        }else{
            // menjalan function err
            return err();
        }


}


  function sortData3(data){

    // Inisialisasi array kosong 
    const result = [];
    // Looping untuk mengeluarkan data array
    for (let  i = 0; i < data.length; i++) {
        // warna mata biru dan age nya diantara 35 sampai dengan 40, dan favorit buah nya apel 

      if(data[i].eyeColor === "blue"){

        if (data[i].age >= 35 && data[i].age <= 45){

          result.push(data[i]);

        }

      }

    }
        // Pengecekan apakah result memiliki isi
    if (result.length) {
        // mengembalikan nilai dataArray diubah ke json
        // return JSON.stringify(result);
        // Mengembalikan nilai array karena akan di tampilkan di table
        return result;

    }else{
        // menjalan function err
        return err();
    }

}


function sortData4(data){

    // Inisialisasi array kosong 
    const result = [];
    // Looping untuk mengeluarkan data array
    for (let  i = 0; i < data.length; i++) {
      // company nya Pelangi atau Intel, dan warna mata nya hijau

      if(data[i].company === "Pelangi" || data[i].company === "Intel"){

        if (data[i].eyeColor === "green"){

          result.push(data[i]);

        }

      }

    }
    // Pengecekan apakah result memiliki isi
    if (result.length) {
        // mengembalikan nilai dataArray diubah ke json
        // return JSON.stringify(result);
        // Mengembalikan nilai array karena akan di tampilkan di table
        return result;

    }else{
        // menjalan function err
        return err();
    }

}


function sortData5(data){

    // Inisialisasi array kosong 
    const result = [];
    // Looping untuk mengeluarkan data array
    for (let  i = 0; i < data.length; i++) {
        // deklarasi variabel years sebagai variabel yg menyimpan hasil potongan tahun

        // variabel remaining menyimpan sisa data setelah - 

        const [years, remaining] = data[i].registered.split("-");

        // registered di bawah tahun 2016 dan masih active(true)

        if( years < 2016 && data[i].isActive){

            result.push(data[i]);

        }

    }
    // Pengecekan apakah result memiliki isi
    if (result.length) {
        // mengembalikan nilai dataArray diubah ke json
        // return JSON.stringify(result);
        // Mengembalikan nilai array karena akan di tampilkan di table
        return result;

    }else{
        // menjalan function err
        return err();
    }

}

// export fungsi

  module.exports =  {sortData1, sortData2, sortData3, sortData4, sortData5};

